import logging
import json
import requests
import sys
from telegram import Update, InputMediaPhoto
from telegram.ext import Updater, CallbackContext, CommandHandler

class ImageDownloadException(Exception):
    """Exception for getting image URL (only HTTP errors)."""
    pass

class Bot:
    """Bot object.
    Insert your own token as parameter in command line. If you don't have it, talk with @BotFather
    After creating Bot object you should setup his commends (use method setup) and run.
    Methods __getImageURL, __welcomeMessage, __sendErrorMessage and __moreCommand using only inside
    object, so please, don't call them. When your bot starts, you can test right send method by
    sending message to bot \test_pic, \test_gif or \test_mp4 (video converts to gif for some reason).
    """

    def __init__(self, token: str):
        self.updater = Updater(token=token, use_context=True)
        self.dispatcher = self.updater.dispatcher

    def setup(self):
        start_handler = CommandHandler(
            'start', lambda update, context: self.__welcomeMessage(update, context))
        more_handler = CommandHandler(
            'more', lambda update, context: self.__moreCommand(update, context, None))
        test_pic_handler = CommandHandler(
            'test_pic', lambda update, context: self.__moreCommand(update, context, 'pic'))
        test_gif_handler = CommandHandler(
            'test_gif', lambda update, context: self.__moreCommand(update, context, 'gif'))
        test_mp4_handler = CommandHandler(
            'test_mp4', lambda update, context: self.__moreCommand(update, context, 'mp4'))
        self.dispatcher.add_handler(start_handler)
        self.dispatcher.add_handler(more_handler)
        self.dispatcher.add_handler(test_pic_handler)
        self.dispatcher.add_handler(test_gif_handler)
        self.dispatcher.add_handler(test_mp4_handler)

    def run(self):
        self.updater.start_polling()
        self.updater.idle()
        self.updater.stop()

    def __getImageURL(self):
        res = requests.get("https://random.dog/woof.json")
        if res.status_code == 200:
            return res.json()['url']
        else:
            raise ImageDownloadException('HTTP error {}'.format(str(res.status_code)))

    def __welcomeMessage(self, update: Update, context: CallbackContext):
        context.bot.send_message(chat_id=update.effective_chat.id,
                                text="Hello! I can send you images with dogs, just send /more")

    def __sendErrorMessage(self, update: Update, context: CallbackContext, message: str):
        message = "I couldn't send to you image, because" + message
        context.bot.send_message(chat_id=update.effective_chat.id, text=message)

    def __moreCommand(self, update: Update, context: CallbackContext, test=None):
        try:
            if not test:
                image_url = self.__getImageURL()
            else:
                if test == 'pic':
                    image_url = 'https://random.dog/00564ba3-e5cb-4b2b-8d97-c65a9ef26c23.png'
                elif test == 'gif':
                    image_url = 'https://random.dog/89ea35da-8190-4161-ba4f-b41882755654.gif'
                elif test == 'mp4':
                    image_url = 'https://random.dog/rSeiGNUY9BudTNar_YjtQFyvq4yLP5jaPjXR3jlubLU.mp4'
        except ImageDownloadException as e:
            self.__sendErrorMessage(update, context, " I've got {}".format(str(e)))
        except requests.Timeout:
            self.__sendErrorMessage(update, context, " I waited too long.")
        except requests.exceptions.ConnectionError:
            self.__sendErrorMessage(update, context, " I have problems with connection.")
        except (requests.exceptions.ChunkedEncodingError, requests.exceptions.ReadTimeout):
            self.__sendErrorMessage(update, context, " image was corrupted.")
        except (Exception, BaseException) as e:
            self.__sendErrorMessage(update, context,
                            "... I don't know, maybe you can understand this: {}".format(str(e)))
        except:
            self.__sendErrorMessage(update, context, "... I don't know. Just don't know.")
        else:
            if image_url.endswith('.mp4'):
                context.bot.send_video(chat_id=update.effective_chat.id, video=image_url)
            elif image_url.endswith('.gif'):
                context.bot.send_animation(chat_id=update.effective_chat.id, animation=image_url)
            else:
                context.bot.send_photo(chat_id=update.effective_chat.id, photo=image_url)


if __name__ == "__main__":
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                         level=logging.INFO)
    print('Press CTRL+C to stop bot.')
    bot = Bot(sys.argv[1])
    bot.setup()
    bot.run()
